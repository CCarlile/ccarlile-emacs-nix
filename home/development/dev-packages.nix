{ pkgs, ... }:

with pkgs;

[
  silver-searcher
  metals
  cowsay
  fortune
  ammonite
  elmPackages.elm-language-server
  gotop
  sqlite
  tree
  kakoune
  sbt
  wget
]
