# My Emacs Setup


## History

When I got into using Linux in college, I found that a keyboard-driven workflow was far faster than using a mouse for many tasks. A big problem is that most major desktop environments (including the ones shipped with macOS and Windows) still place a large emphasis on using the mouse. As a result, configuring my preferred keyboard-driven environment on a new machine always involved some amount of manual configuration. I was taught vim in college, so originally this workflow was centered around vim and some window management influenced by its keybindings. Tracking the configuration was mainly a task of wrangling configuration scripts for a variety of different unix utitlites. Since vim is a unix utility - which means it does one thing (text editing) and does it (very) well - it didn't help me when I needed to keep track of context across a variety of projects.

Emacs comes from a different pedigree - instead of Unix DNA, Emacs is a modern incarnation of a Lisp machine. It's core is a Lisp interpreter, and folks have written a lot of packages for it, including a very powerful vim-like text editing layer (evil), a best-in-class git porcelian (Magit), and a featureful organization and todo system (org-mode). This enables a unified compuiting environment that is keyboard driven and whose guts are open for inspecting and hacking. It even has a self-documenting help system that is very useful for figuring out how it works. Emacs has a package manager available that enables a declarative configuration, so *in theory* it's the only thing that needs to be tracked to reproduce a configuration on a new machine.

Except it's not, really. My workflow reaches out to some command-line tools still - for example, `ag` is a replacement for `grep` that I use to quickly search for a string of text in a directory. It doesn't come preinstalled, so Emacs will get an error the first time I try to search for something. I can install it using the native package manager, but I have to rinse and repeat every time I try and access a dependency that's not available.

Nix is a solution to that problem. Nix is a functional programming language for the domain of building and packaging software. On the file system, packages aren't identified by name or version, but by the cryptographic hash of the Nix expression used to build them. This enables a number of benefits, but in my use case I use it to scaffold my user environment. Another use case is that nix can build shells based on nix expressions, so that dependencies for a project (say, a custom java version with specific certificates in the keystore) can be expressed reproducibly and stored in version control - someone can begin working on a new project with just one command.


## Setup features


### Emacs

-   Managed as a nix expression instead of a lisp blob. This lets individual emacs packages be managed by nix, which means all emacs package dependencies are available on first startup.
-   Space driven leader setup, with key hints. This is a pretty common paradigm in vim/emacs land - popularly featured in the Spacemacs emacs distribution. Since I'm using a vim layer, the space bar does nothing in Normal Mode. I use the space bar to open up a command tree - so for instance my git commands are prefixed with `g` - when I want to **git** log for a **file**, I type `SPC g f`, where I can see the **git** **status** with `SPC g s`. A little window pops up at the bottom as I type them, so I can remember which commands are where if I'm trying to use one that isn't in my muscle memory.
-   Projectile/ag are two packages that work in concert that I use to navigate codebases. Projectile keeps tracks of git projects I've interacted with, and ag enables fast searching within those projects. The super important keybinds I use are:
    -   `SPC s f` - search in a directory
    -   `SPC s p` - search in project files
    -   `SPC p f` - open a file in this project
    -   `SPC p p` - open a file in another project
-   browse-at-remote is another navigation package that opens a browser link to the VC remote of the file, branch, and line I'm currently visiting. Useful for sharing code over Slack.
-   dumb-jump is a package that uses search templates for various languages to provide go-to-definition functionality without any other special software.
-   fuzzy completion with helm - all of the above commands that take input can get a list of available candidates from Emacs. Helm is a completion framework for fuzzy searching, and provides useful visual feedback as I type.
-   LSP / metals - all of the above work well for language-agnostic text editing. Metals is a language server for Scala that enables some scala-specific features, like inline documentation and feedback from the compiler for errors and completion. The `metals` binary is installed and managed by Nix.


### Zsh

My zsh user environment is pretty simple - it enables oh-my-zsh, sets some basic configuration around completion, and enables the `fzf` plugin. I use `fzf` to do a fuzzy search of my command line history, useful for remembering what that one command was that one time. Also, every time you open a terminal, a talking cow gives you a fortune.


### Git

This setup manages my git user and global ignores, so emacs-specific swap files don't make their way into VC.


### Vim

Still, after all these years, sometimes I just need to edit a file on the command line. My main vim preferences are using the `jk` key combination to escape insert mode and swapping `;` and `:` .


### Nix integration

In zsh, nix manages itself. In emacs, we make sure the current nix profile is set on the binary executable path. I also use `direnv`, which enables a per-directory environment variables. In projects that are managed by nix, direnv automatically loads project dependencies using nix when a project file is visited in emacs or a directory is visited in the shell.


## How to install

Instructions for MacOS:

-   Clone the repo
-   Install Nix
-   Install Nix-Darwin
-   Add the home-manager channel
-   Edit `home/user.nix` to your user values, and edit `darwin-configuration.nix` to be like so:
    
    ```nix
    
    { config, pkgs, ... }:
    let
      fonts = import ./home/pretty/fonts.nix;
      dev-packages = import ./home/development/dev-packages.nix;
      me = import ./home/user.nix;
    in
    {
    
      imports = [
        <home-manager/nix-darwin>
      ];
    
      # edit me!
      users.users.ccarlile = {
        name = me.username;
        home = me.homeDir;
      };
    
      # also me!
      home-manager.users.ccarlile = {
        imports = [
          ./home/development/emacs/emacs-init.nix
          ./home/development/emacs/emacs.nix
          ./home/home.nix
        ];
        programs.emacs = {
          package = pkgs.emacsMacport;
        };
        home.packages = fonts pkgs ++ dev-packages pkgs;
      };
    
      fonts.fonts = fonts pkgs;
    
      programs.zsh.enable = true;
    
    }
    
    ```
